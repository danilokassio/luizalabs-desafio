# Luizalabs

Funcionalidade de favoritos par Magazine Luiza. Projeto idealizado para atender ao desafio do processo seletito do Luizalaba.
Este projeto foi desenvolvido pensando numa solução mais próxima da realidade possível com tecnologias novas e recursos que ajudam a mandar a qualidade do código.

## Projetos

- **Great wall of China:** [API Gateway](/great-wall-of-china/readme.md) protege a API autenticando através do servidor de autenticação (The Key Master).
- **Gollum:** [Favorite Service](/gollum/readme.md) API responsável pela feature de favoritar produtos. (GraphQL)
- **Atena:** [Authentication server](/the-key-master/readme.md) servidor OAuth2 responsável pela autenticação.

## DevOps

- Docker
- Docker compose

## Build e executando os serviços

```shell
$ docker-compose build && docker-compose up -d
```

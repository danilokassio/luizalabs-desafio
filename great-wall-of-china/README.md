# Great Wall of China

A icônica muralha da China foi construída para proteger o império de invasões. Este projeto leva este nome justamente por proteger os serviços posicionados atrás dele.

## Stack

 - Node JS 10
 - Docker
 - Server: ```express```
 - Proxy: ```express-http-proxy```
 - Client oAuth2: ```simple-oauth2```

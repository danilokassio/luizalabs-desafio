const express = require('express');
const httpProxy = require('express-http-proxy');
const app = express();
const port = 5000;
const {
  FAVORITE_API_URL,
  OAUTH2_URL,
} = require('./URLs');
 
const favoriteServiceProxy = httpProxy(FAVORITE_API_URL);
const oauthServiceProxy = httpProxy(OAUTH2_URL);

app.get('/', (req, res) => res.send('Gateway API'));

app.get('/graphql', (req, res, next) => favoriteServiceProxy(req, res, next));
app.post('/graphql', async (req, res, next) => {
  
  const credentials = {
    client: {
      id: req.headers.clientid,
      secret: req.headers.clientsecret
    },        
    auth: {
      tokenHost: `http://${OAUTH2_URL}`,
      tokenPath: '/oauth/token',
      authorizePath: '/authorise',
    }
  };
  // Initialize the OAuth2 Library
  const oauth2 = require('simple-oauth2').create(credentials);
  const tokenConfig = {
    scope: 'profile',
  };
  try {
    const result = await oauth2.clientCredentials.getToken(tokenConfig);
    const accessToken = oauth2.accessToken.create(result);
    console.log('accessToken', accessToken.token.accessToken);
    req.headers.authorization = accessToken.token.accessToken;
  } catch (error) {
    console.log('Access Token error', error.message);
    next(error);
  }
  
  return favoriteServiceProxy(req, res, next)
  }
);
app.get('/oauth', (req, res, next) => oauthServiceProxy(req, res, next));

app.listen(port, () => console.log(`🚧 API Gateway listening on port ${port}!`));

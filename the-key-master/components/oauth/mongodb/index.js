/** https://github.com/dsquier/oauth2-server-php-mysql **/
const config = require('../../../config')
const mongoose = require('mongoose')

console.log('MONGODB -> ', config.mongo.uri)
mongoose.connect(config.mongo.uri, function(err) {
  if (err) return console.log(err);
  console.log('Mongoose Connected');
});
const db ={};
db.OAuthAccessToken = require('./OAuthAccessToken')
db.OAuthAuthorizationCode = require('./OAuthAuthorizationCode')
db.OAuthClient = require('./OAuthClient')
db.OAuthRefreshToken = require('./OAuthRefreshToken')
db.OAuthScope = require('./OAuthScope')
db.User = require('./User')

module.exports = db;
/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
const bcrypt = require('bcryptjs');
const config = require('../../config')
const mongodb = require('./mongodb');

const OAuthAccessToken = mongodb.OAuthAccessToken
const OAuthAuthorizationCode = mongodb.OAuthAuthorizationCode
const OAuthClient = mongodb.OAuthClient
const OAuthRefreshToken = mongodb.OAuthRefreshToken
const OAuthScope = mongodb.OAuthScope
const User = mongodb.User

OAuthScope.find({}).remove()
  .then(function() {
    OAuthScope.create({
        scope: 'profile',
        is_default: false
      },{
        scope: 'defaultscope',
        is_default: true
      })
      .then(function() {
        console.log('finished populating OAuthScope');
      });
  });
const salt = bcrypt.genSaltSync(10);
const hash = bcrypt.hashSync("admin", salt);
User.find({}).remove()
  .then(function() {
    User.create({
        username: 'admin',
        password: hash
      })
      .then(function(user) {
        console.log('finished populating users',user);
        return OAuthClient.find({}).remove()
          .then(function() {
            OAuthClient.create({
                client_id:'democlient',
                client_secret:'democlientsecret',
                redirect_uri:'http://localhost/cb',
                User:user._id
              })
              .then(function(client) {
                console.log('finished populating OAuthClient',client);
              }).catch(console.log);
          });
          
      });
  });


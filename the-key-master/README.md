# The Key Master

Supports `"oauth2-server": "^3.0.0-b2",`

## Quick Start

Para simular, Postman: https://www.getpostman.com/collections/37afd82600127fbeef28

## Features

- Suporta authorization_code, password, refresh_token, client_credentials e extension (custom) grant types.
- Suporta diversas formas de armazenamento e.g. PostgreSQL, MySQL, Mongo, Redis...
- Implementado com mongodb.

## Stack

 - Node JS 10
 - Docker
 - Express
 - Server oAuth2: ```oauth2-server```

 ## Dados Iniciais

 ```
 {
  "clientId": "democlient",
  "clientSecret": "democlientsecret"
}
```
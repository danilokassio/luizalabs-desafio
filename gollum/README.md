# Gollum

API de produtos favoritos da Magazine Luiza.

Gollum é um personagem fictício das obras do filólogo e professor britânico J. R. R. Tolkien. Este projeto leva seu nome por conta de seu fascínio pelo seu 'precioso', o anel do poder, fazendo uma alusão do comportamento dos clientes ao favoritar os produtos dentro o ecommerce da Magazine Luiza.

## Doc

[GraphQL Docs (tab DOCS / SCHEMA)](http://localhost:4000)

## Stack

 - Javascript (NodeJs)
 - GraphQL (Apollo)
 - MongoDB
 - Redis

## Code Quality

 - Jest (testes unitários)
 - Chakram (REST API testing)
 - apollo-server-testing (GraphQL testing)

## GraphQL QUERIES

Informar o Client Id e o Client Secret no headers
```
{
  "clientId": "democlient",
  "clientSecret": "democlientsecret"
}
```

### GetProducts

```
query GetProducts {
  products(page: 1) {
    id
    title
    image
    price
    reviewScore
  }
}
```

### getProduct

```
query getProduct {
  product(id: "1bf0f365-fbdd-4e21-9786-da459d78dd1f"){
   	id
    title
    image
    price
    reviewScore
  }
}
```

### getClients

```
query getClients {
  allClients {
    id
    name
    email
  }
}
```

### insertClient

```
mutation insertClient($client: ClientInput!) {
  addClient(client: $client) {
    success
    message
    clients {
      name
      email
    }
  }
}
```
Obs: Set the variables in the `QUERY VARIABLES` section on GraphQL Playground.

### getClient

```
query getClient($client: ClientQuery!) {
  clients(client: $client) {
    id
    name
    email
  }
}
```

### updateClient

```
mutation updateClient($id: ID!, $client: ClientQuery!) {
  updateClient(id: $id, client: $client)
}
```

### insertClient

```
mutation insertClient($client: ClientInput!) {
  addClient(client: $client) {
    success
    message
    clients {
      name
      email
    }
  }
}
```

### removeClient

```
mutation removeClient($client: ClientQuery!) {
  removeClient(client: $client)
}
```

### favoriteProduct

```
mutation favoriteProduct($client: ID!, $product: ID!) {
  favoriteProduct(client: $client, product: $product) {
    success
    message
    favorite {
      id
      client {
        id
        name
        email
      }
      products {
        id
        title
        image
        price
        reviewScore
      }
    }
  }
}
```

### Favorites

```
query Favorites {
  favorites (client: "5d83b5f2f79e598a56be9df4") {
    id
    client {
      id
      name
      email
    }
    products {
      id
      title
      image
      price
      reviewScore
    }
  }
}
```

### disfavoriteProduct

```
mutation disfavoriteProduct($client: ID!, $product: ID!) {
  disfavoriteProduct(client: $client, product: $product) {
    success
    message
    favorite {
      id
      client {
        id
        name
        email
      }
      products {
        id
        title
        image
        price
        reviewScore
      }
    }
  }
}
```
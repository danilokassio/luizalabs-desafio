const ProductAPI = require("../product");

/**
 * There are mock Products at the bottom of this file.
 * 1 mock for the RAW API reponse, and another
 * for the shape of the lauch after it would have been
 * transformed by the product reducer.
 */

const mocks = {
  get: jest.fn(),
};

const ds = new ProductAPI();
ds.get = mocks.get;

describe("[ProductAPI.productReducer]", () => {
  it("properly transforms product", () => {
    expect(ds.productReducer(mockProductResponse.products[0])).toEqual(mockProduct);
  });
});

describe("[ProductAPI.getAllProducts]", () => {
  it("looks up products from api", async () => {
    mocks.get.mockReturnValueOnce(mockProductResponse);
    const res = await ds.getAllProducts();

    expect(res).toEqual([mockProduct]);
  });
});

describe("[ProductAPI.getProductById]", () => {
  it("should look up single product from api", async () => {
    mocks.get.mockReturnValueOnce(mockProductResponse.products[0]);
    const res = await ds.getProduct("1bf0f365-fbdd-4e21-9786-da459d78dd1f");

    expect(res).toEqual(mockProduct);
  });
});

/**
 * MOCK DATA BELOW
 */

// properly transformed product (Obs.: In this case the transformed product has the same formmat from the API)
const mockProduct = {
  price: 1699,
  image: "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg",
  brand: "bébé confort",
  id: "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
  title: "Cadeira para Auto Iseos Bébé Confort Earth Brown",
};

// raw product response from API
const mockProductResponse = {
  products: [
    {
      price: 1699,
      image: "http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg",
      brand: "bébé confort",
      id: "1bf0f365-fbdd-4e21-9786-da459d78dd1f",
      title: "Cadeira para Auto Iseos Bébé Confort Earth Brown",
    },
  ],
};

module.exports = mockProductResponse;

const { MongoDataSource } = require("apollo-datasource-mongodb");

class ClientDB extends MongoDataSource {
  constructor({ clients }) {
    super();
    this.clients = clients;
  }

  getClient(userId) {
    return this.findOneById(userId);
  }
}

module.exports = ClientDB;

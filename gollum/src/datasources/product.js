const { RESTDataSource } = require("apollo-datasource-rest");

class ProductAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = "http://challenge-api.luizalabs.com/api/product/";
  }

  productReducer(product) {
    /**
     * There is no diference between format from API to our GraphQL response.
     * If nedd some customization here is the place to apply those changes.
     **/

    return { ...product };
  }

  async getAllProducts(page) {
    const response = await this.get(`?page=${page}`);

    return response && Array.isArray(response.products)
      ? response.products.map(product => {
          return this.productReducer(product);
        })
      : [];
  }

  async getProduct(id) {
    const response = await this.get(`${id}`);
    return this.productReducer(response);
  }
}

module.exports = ProductAPI;

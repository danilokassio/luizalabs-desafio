const { ApolloServer } = require("apollo-server");
const { RedisCache } = require("apollo-server-cache-redis");
const fetch = require("node-fetch");

const typeDefs = require("./schema");
const resolvers = require("./resolvers");

const ProductAPI = require("./datasources/product");

const config = require("./config");

const getUser = async token => {
  console.log("TOKEN ->", token);
  return fetch(`http://${config.oauth.uri}:3000/profile`, {
    method: "get",
    headers: { Authorization: `Bearer ${token}` },
  })
    .then(res => res.json())
    .then(json => {
      return json.profile.User || null;
    });
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const token = req.headers.authorization || "";

    const user = await getUser(token);

    if (!user) throw new AuthenticationError("Para acessar é necessário estar autenticado.");

    return { user };
  },
  cache: new RedisCache({
    connectTimeout: 5000,
    socket_keepalive: false,
    host: config.redis.uri,
  }),
  dataSources: () => ({
    productAPI: new ProductAPI(),
  }),
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

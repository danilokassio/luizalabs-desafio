const { ApolloError } = require("apollo-server");
const { Client, Favorite } = require("../db/mongoose");
const { validateClient, validateFavorite, validateDisfavorite } = require("../validations");
const config = require("../config");

module.exports = {
  Query: {
    products: (_, __, { dataSources }) => dataSources.productAPI.getAllProducts(__.page),
    product: (_, __, { dataSources }) => dataSources.productAPI.getProduct(__.id),
    allClients: async (_, __, { dataSources }) => await Client.find({}),
    clients: async (_, __, { dataSources }) => {
      return await Client.find(__.client.id ? { _id: __.client.id } : { ...__.client });
    },
    favorites: async (_, __, { dataSources }) => {
      const favoriteDB = await Favorite.findOne({ client: __.client }).populate("client");
      if (!favoriteDB) {
        return null;
      }

      const pArray = favoriteDB.products.map(async product => {
        return await dataSources.productAPI.getProduct(product);
      });
      const products = await Promise.all(pArray);

      return {
        id: favoriteDB.id,
        client: favoriteDB.client,
        products,
      };
    },
  },

  Mutation: {
    // CLIENT
    addClient: async (_, __) => {
      const validated = await validateClient(__.client);

      if (validated.success) {
        const client = await Client.create(__.client);
        return {
          success: false,
          message: `Cliente ${__.client.name} cadastrado com sucesso.`,
          clients: [client],
        };
      }

      return validated;
    },

    updateClient: async (_, __) => {
      const validated = await validateClient(__.client);

      if (validated.success) {
        const response = await Client.update({ _id: __.id }, { $set: __.client }, null, function(err) {
          if (err) {
            console.error(err);
            throw new ApolloError("Error from database", err);
          }
        });
        return response.n;
      }
      return validated.message;
    },

    removeClient: async (_, __) =>
      await Client.find(__.client)
        .remove()
        .exec()
        .then(response => response.deletedCount),

    // FAVORITES
    favoriteProduct: async (_, __, { dataSources }) => {
      const validated = await validateFavorite(__, dataSources.productAPI);
      if (validated.success) {
        validated.favorite.client = __.client;
        validated.favorite.products.includes(__.product)
          ? validated.favorite.products
          : validated.favorite.products.push(__.product);
        await Favorite.create(validated.favorite);
        favorite = await Favorite.findOne({ client: __.client }).populate("client");
        const pArray = favorite.products.map(async product => {
          return await dataSources.productAPI.getProduct(product);
        });
        const products = await Promise.all(pArray);

        return {
          success: true,
          message: `Produto ${__.product} adicionado aos favoritos.`,
          favorite: {
            id: favorite.id,
            client: favorite.client,
            products,
          },
        };
      }

      return validated;
    },

    disfavoriteProduct: async (_, __, { dataSources }) => {
      const validated = await validateDisfavorite(__, dataSources.productAPI);
      if (validated.success) {
        validated.favorite.client = __.client;
        validated.favorite.products = validated.favorite.products.filter(product => product != __.product);

        await Favorite.create(validated.favorite);

        favorite = await Favorite.findOne({ client: __.client }).populate("client");
        const pArray = favorite.products.map(async product => {
          return await dataSources.productAPI.getProduct(product);
        });
        const products = await Promise.all(pArray);

        return {
          success: true,
          message: `Produto ${__.product} removido dos favoritos.`,
          favorite: {
            id: favorite.id,
            client: favorite.client,
            products,
          },
        };
      }

      return validated;
    },
  },
};

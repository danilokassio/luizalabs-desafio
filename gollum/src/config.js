module.exports = {
  mongo: {
    uri: "mongodb://mongo:27017",
    db: "gollum",
  },
  redis: {
    uri: "redis",
  },
  oauth: {
    uri: "auth-service",
  },
  seedDB: true,
  favorite: {
    maxProducts: 8,
  },
};

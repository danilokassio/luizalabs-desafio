const { gql } = require("apollo-server");

const typeDefs = gql`
  type Query {
    products(page: Int!): [Product]!
    product(id: ID!): Product
    allClients: [Client]!
    clients(client: ClientQuery!): [Client]
    favorites(client: ID!): Favorite
  }

  type Mutation {
    addClient(client: ClientInput!): ClientQueryResponse

    #Return quantity of updated clients
    updateClient(id: ID!, client: ClientQuery!): String

    #Return quantity of removed clients
    removeClient(client: ClientQuery!): String

    favoriteProduct(client: ID!, product: ID!): FavoriteUpdateResponse!

    disfavoriteProduct(client: ID!, product: ID!): FavoriteUpdateResponse!
  }

  type Product {
    id: ID!
    title: String!
    image: String!
    price: Float!
    reviewScore: Float
  }

  input ProductInput {
    id: ID!
    title: String!
    image: String!
    price: Float!
    reviewScore: Float
  }

  type ClientQueryResponse {
    success: Boolean!
    message: String
    clients: [Client]
  }

  type Client {
    id: ID!
    name: String
    email: String
  }

  input ClientInput {
    name: String!
    email: String!
  }

  input ClientQuery {
    id: String
    name: String
    email: String
  }

  type Favorite {
    id: ID!
    client: Client
    products: [Product]
  }

  input FavoriteInput {
    clientId: String
    product: String
  }

  type FavoriteUpdateResponse {
    success: Boolean!
    message: String
    favorite: Favorite
  }
`;

module.exports = typeDefs;

"use strict";

const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const ClientSchema = new Schema({
  name: String,
  email: String,
});

module.exports = mongoose.model("Client", ClientSchema);

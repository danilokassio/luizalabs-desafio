"use strict";

const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

const FavoriteSchema = new Schema({
  client: { type: Schema.Types.ObjectId, ref: "Client" },
  products: [String],
});

module.exports = mongoose.model("Favorite", FavoriteSchema);

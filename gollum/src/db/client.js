const { MongoDataSource } = require("apollo-datasource-mongodb");

class ClientDB extends MongoDataSource {
  getClient(userId) {
    return this.findOneById(userId);
  }
}

module.exports = ClientDB;

const mongoose = require("mongoose");
const config = require("../config");

mongoose.connect(
  `${config.mongo.uri}/${config.mongo.db}`,
  { useUnifiedTopology: true, useNewUrlParser: true },
  function(err) {
    if (err) return console.log(err);
    console.log("Mongoose Connected");
  },
);
const db = {};

db.Client = require("./schema/Client");
db.Favorite = require("./schema/Favorite");
db.mongoose = mongoose;

module.exports = db;

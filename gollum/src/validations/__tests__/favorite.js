const { Favorite, mongoose } = require("../../db/mongoose");
const { validateFavorite } = require("../favorite");
const ProductAPI = require("../../datasources/product");
const mockLaunchResponse = require("../../datasources/__tests__/product");

const ds = new ProductAPI();
ds.get = jest.fn(() => [mockLaunchResponse]);

describe("validations/favorite", () => {
  beforeAll(done => {
    mongoose.disconnect();
    return done();
  });

  it("validate new favorite", async () => {
    const mock = jest.spyOn(Favorite, "findOne");
    mock.mockImplementation(() => Promise.resolve(null));

    const mockFavorite = {
      client: "mockedID",
      product: "mockedIDProduct",
    };

    const mockReturn = {
      success: true,
      favorite: { products: [] },
    };

    const validatedFavorite = await validateFavorite(mockFavorite, ds);
    expect(validatedFavorite).toEqual(mockReturn);
  });
});

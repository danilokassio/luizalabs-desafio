const { Client, mongoose } = require("../../db/mongoose");
const clientValidator = require("../client");

describe("validations/client", () => {
  beforeAll(done => {
    mongoose.disconnect();
    return done();
  });
  it("validate new client", async () => {
    const mock = jest.spyOn(Client, "findOne");
    mock.mockImplementation(() => Promise.resolve(null));

    const mockClient = {
      name: "danilo",
      email: "danilo@email.com",
    };

    const mockReturn = { success: true };

    const validatedClient = await clientValidator(mockClient);
    expect(validatedClient).toEqual(mockReturn);
  });

  it("validate client already registered", async () => {
    const mock = jest.spyOn(Client, "findOne");
    mock.mockImplementation(() =>
      Promise.resolve({
        name: "danilo",
        email: "danilo@email.com",
      }),
    );

    const mockClient = {
      name: "danilo",
      email: "danilo@email.com",
    };

    const mockReturn = {
      success: false,
      message: `Email ${mockClient.email} já cadastrado.`,
      clients: null,
    };

    const validatedClient = await clientValidator(mockClient);
    expect(validatedClient).toEqual(mockReturn);
  });

  it("validate client with bad format email", async () => {
    const mock = jest.spyOn(Client, "findOne");
    mock.mockImplementation(() =>
      Promise.resolve({
        success: false,
        message: "O e-mail não foi digitado corretamente.",
        clients: null,
      }),
    );

    const mockClient = {
      name: "danilo",
      email: "badformatemail",
    };

    const mockReturn = {
      success: false,
      message: "O e-mail não foi digitado corretamente.",
      clients: null,
    };

    const validatedClient = await clientValidator(mockClient);
    expect(validatedClient).toEqual(mockReturn);
  });
});

const validateClient = require("./client");
const { validateFavorite, validateDisfavorite } = require("./favorite");

module.exports = {
  validateClient,
  validateFavorite,
  validateDisfavorite,
};

const { Favorite, Client } = require("../db/mongoose");
const config = require("../config");

const validateFavorite = async (favorite, productAPI) => {
  try {
    await checkClient(favorite.client);
  } catch (err) {
    return err;
  }

  try {
    const product = await productAPI.getProduct(favorite.product);
  } catch (err) {
    return {
      success: false,
      message: `Produto ${favorite.product} não existe. ${err}`,
      favorite: null,
    };
  }

  const favoriteDB = (await Favorite.findOne({ client: favorite.client })) || { products: [] };

  if (favoriteDB.products.length >= config.favorite.maxProducts) {
    return {
      success: false,
      message: `Já existem ${config.favorite.maxProducts} produtos favoritados.`,
    };
  }

  const productAlreadyFavorited =
    favoriteDB.products.length > 0 ? favoriteDB.products.find(product => product === favorite.product) : false;
  if (productAlreadyFavorited) {
    return {
      success: false,
      message: `Produto ${favorite.product} já está favoritado.`,
      favorite: null,
    };
  }

  return {
    success: true,
    favorite: favoriteDB,
  };
};

const validateDisfavorite = async (favorite, productAPI) => {
  try {
    await checkClient(favorite.client);
  } catch (err) {
    return err;
  }

  const favoriteDB = (await Favorite.findOne({ client: favorite.client })) || { products: [] };

  if (favoriteDB.products.length == 0 || !favoriteDB.products.find(product => product === favorite.product)) {
    return {
      success: false,
      message: `O produto ${favorite.product} não estava favoritado.`,
    };
  }

  return {
    success: true,
    favorite: favoriteDB,
  };
};

const checkClient = async client => {
  try {
    const clientFromDB = await Client.findById(client);
  } catch (err) {
    return {
      success: false,
      message: `Client ${client} não existe. ${err}`,
      clients: null,
    };
  }
  return {
    success: true,
  };
};

module.exports = {
  validateFavorite,
  validateDisfavorite,
};

const { Client } = require("../db/mongoose");

module.exports = async client => {
  const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  /**
   * This validation works for insert and update
   * In insert the schema validates if the email exists
   * */
  if (client.email && !emailRegexp.test(client.email)) {
    return {
      success: false,
      message: "O e-mail não foi digitado corretamente.",
      clients: null,
    };
  }

  const clientFromDB = await Client.findOne({ email: client.email });
  if (clientFromDB) {
    return {
      success: false,
      message: `Email ${client.email} já cadastrado.`,
      clients: null,
    };
  }

  return { success: true };
};
